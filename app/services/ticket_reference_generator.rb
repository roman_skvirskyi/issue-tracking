class TicketReferenceGenerator
  def initialize(reference = nil)
    @reference = reference
  end

  def reference
    @reference ||= generate_reference
  end

  def regenerate
    @reference = nil
  end

  private

  def generate_reference
    "#{random_string}-#{random_hex}-#{random_string}-#{random_hex}-#{random_string}"
  end

  def random_string
    SecureRandom.urlsafe_base64(2)
  end

  def random_hex
    SecureRandom.hex(1)
  end
end