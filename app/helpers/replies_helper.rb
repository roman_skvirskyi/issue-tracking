module RepliesHelper
  def new_owner(reply)
    reply.new_owner.try(:email) || t('replies.owner_unassigned')
  end
end