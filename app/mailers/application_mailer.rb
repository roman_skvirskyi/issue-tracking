class ApplicationMailer < ActionMailer::Base
  default from: 'support@issue_tracking.com'
  layout 'mailer'
end
