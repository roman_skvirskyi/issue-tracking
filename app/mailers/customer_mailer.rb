class CustomerMailer < ApplicationMailer
  def new_ticket(ticket)
    @ticket = ticket
    @customer = @ticket.creator

    mail(to: @customer.email, subject: 'New ticket')
  end

  def updated_ticket(ticket)
    @ticket = ticket
    @customer = @ticket.creator

    mail(to: @customer.email, subject: 'Updated ticket')
  end
end
