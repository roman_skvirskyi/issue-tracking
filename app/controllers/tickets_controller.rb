class TicketsController < ApplicationController
  before_action :authenticate_user!

  before_action :find_ticket, except: %i(index search)

  def index
    @tickets = case params[:tickets_type]
      when 'on_hold'
        Ticket.on_hold
      when 'open'
        Ticket.open
      when 'closed'
        Ticket.closed
      else
        Ticket.all
      end

    @tickets = @tickets.paginate(page: params[:page])
  end

  def edit

  end

  def update
    @ticket.assign_attributes ticket_params

    if @ticket.save
      flash[:success] = t('tickets.update.success')

      redirect_to ticket_path(@ticket.id)
    else
      flash[:alert] = @ticket.errors.full_messages.join(', ')

      render :edit
    end
  end

  def show

  end

  def search
    @tickets = Ticket.search do
      fulltext params[:query]
    end.results
  end

  private

  def ticket_params
    params.require(:ticket).permit(:title, :description, :owner_id, :status_id)
  end

  def find_ticket
    @ticket = Ticket.find params[:id]
  end
end