class RepliesController < ApplicationController
  before_action :authenticate_user!
  before_action :find_ticket

  def index
    @replies = @ticket.replies.paginate(page: params[:page])
  end

  def new
    @reply = @ticket.replies.new
  end

  def create
    @reply = @ticket.replies.new(reply_params)

    if @reply.save
      flash[:success] = t('replies.create.success')

      redirect_to ticket_replies_path(@ticket.id)
    else
      flash[:alert] = @reply.errors.full_messages.join(', ')

      render :new
    end
  end

  private

  def find_ticket
    @ticket = Ticket.find(params[:ticket_id])
  end

  def reply_params
    params.require(:reply).permit(:body, :new_status_id, :new_owner_id)
  end
end