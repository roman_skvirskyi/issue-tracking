class Customers::TicketsController < ApplicationController
  before_action :find_customer
  before_action :find_ticket, except: %w(new create)

  def index
    if @customer.tickets.where(reference: params[:reference])
      @tickets = @customer.tickets.paginate(page: params[:page])
    else
      flash[:alert] = t('customers.tickets.index.access_error')
      redirect_to new_customer_ticket_path(@customer)
    end
  end

  def new
    @ticket = Ticket.new
  end

  def create
    @ticket = Ticket.new(ticket_params.merge(creator_id: @customer.id))

    if @ticket.save
      flash[:success] = t('customers.tickets.create.success')
      CustomerMailer.new_ticket(@ticket).deliver_later

      redirect_to new_customer_ticket_path(@customer)
    else
      flash[:alert] = @ticket.errors.full_messages.join(', ')

      render :new
    end
  end

  def show
  end

  def edit
  end

  def update
    @ticket.assign_attributes(ticket_params)

    if @ticket.save
      flash[:success] = t('customers.tickets.update.success')
      CustomerMailer.updated_ticket(@ticket).deliver_later

      redirect_to customer_ticket_path(@customer, @ticket)
    else
      flash[:alert] = @ticket.errors.full_messages.join(', ')

      render :edit
    end
  end

  private

  def find_customer
    @customer = Customer.find(params[:customer_id])
  end

  def find_ticket
    @ticket = @customer.tickets.find_by(reference: params[:id])
  end

  def ticket_params
    params.require(:ticket).permit(:title, :description)
  end
end