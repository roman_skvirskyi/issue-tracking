class CustomersController < ApplicationController
  def new
    @customer = Customer.new
  end

  def create
    @customer = Customer.find_or_initialize_by(customer_params)

    if @customer.save
      flash[:success] = t('customers.create.welcome', name: @customer.name)

      redirect_to new_customer_ticket_path(@customer)
    else
      flash[:danger] = @customer.errors.full_messages.join(', ')

      render :new
    end
  end

  private

  def customer_params
    params.require(:customer).permit(:email, :name)
  end
end