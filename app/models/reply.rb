class Reply < ActiveRecord::Base
  belongs_to :ticket

  belongs_to :new_owner, class_name: 'User'
  belongs_to :new_status, class_name: 'Status'

  validates_presence_of :ticket

  after_create :update_ticket

  private

  def update_ticket
    ticket.update_attributes(owner_id: new_owner_id, status_id: new_status_id)
  end
end