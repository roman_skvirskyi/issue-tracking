class Ticket < ActiveRecord::Base
  validates_uniqueness_of :reference, allow_nil: true

  belongs_to :creator, class_name: 'Customer'
  belongs_to :owner, class_name: 'User'
  belongs_to :status

  has_many :replies

  before_create :generate_reference, :normalize_status

  scope :on_hold, -> { joins(:status).where(statuses: {name: Status::ON_HOLD}) }
  scope :closed, -> { joins(:status).where(statuses: {name: [Status::CANCELLED, Status::COMPLETED]}) }

  searchable do
    text :description, :title, :reference
  end

  def to_param
    reference
  end

  def self.open
    joins(:status)
        .where(statuses: {name: [Status::WAITING_FOR_STAFF, Status::WAITING_FOR_CUSTOMER]}, owner_id: nil)
  end

  private

  def generate_reference
    self.reference = TicketReferenceGenerator.new.reference
  end

  def normalize_status
    self.status ||= default_status
  end

  def default_status
    Status.waiting_for_staff_response
  end
end