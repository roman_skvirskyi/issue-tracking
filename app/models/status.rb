class Status < ActiveRecord::Base
  DEFAULT_STATUSES = ['Waiting for Staff Response', 'Waiting for Customer', 'On Hold', 'Cancelled', 'Completed']

  WAITING_FOR_STAFF = 'Waiting for Staff Response'
  WAITING_FOR_CUSTOMER = 'Waiting for Customer'
  ON_HOLD = 'On Hold'
  CANCELLED = 'Cancelled'
  COMPLETED = 'Completed'

  validates_uniqueness_of :name

  scope :waiting_for_staff_response, -> { find_by(name: WAITING_FOR_STAFF) }
end