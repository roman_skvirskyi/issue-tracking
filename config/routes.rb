Rails.application.routes.draw do
  devise_for :users

  root 'home#index'

  resources :customers, only: %i(new create) do
    resources :tickets, module: 'customers', except: :destroy
  end

  resources :tickets, except: %i(new create destroy) do
    get :search,  on: :collection

    resources :replies, only: %i(index new create)
  end
end
