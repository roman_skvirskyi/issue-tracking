class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.integer :creator_id, null: false
      t.integer :owner_id

      t.belongs_to :status

      t.string :title
      t.text :description
      t.string :reference

      t.timestamps
    end

    add_index :tickets, :reference, unique: true
  end
end
