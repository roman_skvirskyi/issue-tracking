class CreateReplies < ActiveRecord::Migration
  def change
    create_table :replies do |t|
      t.text :body

      t.belongs_to :ticket, null: false

      t.integer :new_status_id
      t.integer :new_owner_id

      t.timestamps
    end
  end
end
